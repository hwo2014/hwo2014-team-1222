var tracks;
var throttle;
var ppos;
var ppos1;
var ppos2;
var ppos3;
var ppos4;
var ppos5;
var pie = Math.PI;
var nowPP;
var oldPP;
var speed;
var info;
var flag;

var util = require("util");
var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    
    //log header print
    if(typeof data.gameTick === "undefined"){
      console.log('-- carPositions');
      //console.log(util.inspect(data,false,null));
      console.log('data.gameTick name lap pieceIndex inPieceDistance startLaneIndex endLaneIndex angle throttle');
    }

    //array search 
    for(key in data.data){

      //my car ?
      if(data.data[key].id.name == botName){

        //-------------------------------------
        //throttle contorol START
        //-------------------------------------
        throttle = 1;

        //now angle
        if(Math.abs(data.data[key].angle) > 3){
          //throttle = throttle -0.2;
        }

        //next pirces is straight or corner ?
        ppos = data.data[key].piecePosition.pieceIndex;
        ppos1 = (ppos+1)%tracks.length;
        ppos2 = (ppos+2)%tracks.length;
        ppos3 = (ppos+3)%tracks.length;
        ppos4 = (ppos+4)%tracks.length;
        ppos5 = (ppos+5)%tracks.length;

        //next is corner
        if(typeof tracks[ppos1].length === "undefined"){
          if(Math.abs(tracks[ppos1].angle) > 40){
            if(speed > 8.5){
              throttle = throttle -0.95;
            }else if(speed > 8.0){
              throttle = throttle -0.90;
            }else if(speed > 7.5){
              throttle = throttle -0.80
            }else if(speed > 7.0){
              throttle = throttle -0.70;
            }else if(speed > 6.5){
              throttle = throttle -0.50;
            }else if(speed > 6.0){
              throttle = throttle -0.30;
            }else if(speed > 5.5){
              throttle = throttle -0.00;
            }else if(speed > 5.0){
              throttle = throttle -0.00;
            }
          }
        }
        

        //next next is corner
        if(typeof tracks[ppos2].length === "undefined"){
        }

        //next corner is ogee ? 
        if(typeof tracks[ppos1].length === "undefined"
          && typeof tracks[ppos2].length === "undefined"){
          if(tracks[ppos1].angle * tracks[ppos2].angle <0){
            throttle = throttle +0.0;
          }
        }

        //speed meter
        oldPP = nowPP;
        nowPP = tracks[ppos].offset
              + data.data[key].piecePosition.inPieceDistance;
        if( nowPP - oldPP >= 0){ // toriaezu!!
          speed = nowPP - oldPP;
        }

        //-------------------------------------
        //throttle contorol END
        //-------------------------------------
      }


      //log print
      if(speed == 0){
        info = 'crash';
      }else{
        info ='';
      }
      console.log(
        ''+data.gameTick
        +' '+data.data[key].id.name
        +' '+data.data[key].piecePosition.lap
        +' '+data.data[key].piecePosition.pieceIndex
        +' '+Math.round(data.data[key].piecePosition.inPieceDistance *1)
        +' '+data.data[key].piecePosition.lane.startLaneIndex
        +' '+data.data[key].piecePosition.lane.endLaneIndex
        +' '+Math.round(data.data[key].angle *1)
        +' '+Math.round(speed *10)
        +' '+Math.round(throttle *100)
        +' '+info
      );

    } 

    //process.exit();


    send({
      msgType: "throttle",
      data: throttle
    });

  } else {
    if (data.msgType === 'join') {
      console.log('-- Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('-- Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('-- Race ended');
		} else if (data.msgType === 'turboAvailable') {
			console.log('-- turboAvailable!!!!!!!!!!!!!!!');

//	    send({
//	      msgType: "turbo",
//	      data: "turboooooooooooooooooooooooo!!!!!!!!!!!!!!!!!!!"
//	    });

    } else if (data.msgType === 'gameInit') {
      console.log('-- gameInit');
      //console.log(util.inspect(data,false,null));

      tracks = data.data.race.track.pieces;

      var offset=0;
      for(key in tracks){
        if(typeof tracks[key].length === "undefined"){
          a = Math.abs(2 * tracks[key].radius * pie * tracks[key].angle / 360);  //toriaezu!!
        }else{
          a = tracks[key].length;
        }
        tracks[key].stride = a;
        tracks[key].offset = offset;
        offset += a;

      }

      console.log(util.inspect(tracks,false,null));
      //process.exit();
    } 

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
